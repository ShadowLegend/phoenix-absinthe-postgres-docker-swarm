FROM elixir:1.6.5

RUN apt-get -y -qq update > /dev/null && \
  mix local.hex --force && \
  mix local.rebar --force && \
  ln -s /lib/x86_64-linux-gnu/libncursesw.so.5  /lib/x86_64-linux-gnu/libncursesw.so.6

WORKDIR /phx_test

COPY phx_test.tar.gz .

RUN tar xfz phx_test.tar.gz

EXPOSE 4001

CMD ["bin/phx_test", "foreground"]

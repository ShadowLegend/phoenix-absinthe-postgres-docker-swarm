#!/bin/bash

if [ -z $1 ] || [ -z $2 ];
    then
        echo 'Usage: ./build.sh <docker-repo-name> <image-name>'
        exit 1
fi

DOCKER_REPO_NAME=$1
IMAGE_NAME=$2
APP_NAME=phx_test
RELEASE_PATH=_build/prod/rel/$APP_NAME/releases

VERSION=$(cat mix.exs \
    | grep version \
    | head -1 \
    | awk -F: '{ print $2 }' \
    | sed 's/[",]//g' \
    | tr -d [:space:])

mix deps.get --only prod
MIX_ENV=prod mix compile
mix phx.digest
mix ecto.migrate
MIX_ENV=prod mix release

cp $RELEASE_PATH/$VERSION/$APP_NAME.tar.gz .

docker image build \
    -t $DOCKER_REPO_NAME/$IMAGE_NAME:$VERSION \
    -f production.Dockerfile .
# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :phx_test,
  ecto_repos: [PhxTest.Repo]

# Configures the endpoint
config :phx_test, PhxTestWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qG99kqbm/r53oDCoscVVnXF5jWVcbMMBGDfNU5f71S3mEUUX762ev7CCFUghagSk",
  render_errors: [view: PhxTestWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhxTest.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

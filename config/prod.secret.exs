use Mix.Config

config :phoenix, :serve_endpoints, true
# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :phx_test, PhxTestWeb.Endpoint,
  http: [port: 4001],
  url: [host: "server", port: 4001],
  secret_key_base: "LUePasF6BXdKJV1sUdaBZXLEvWX1rgdvo3OEs46bp7dq/sVJ06CovxkF3nIMQRSy"

# Configure your database
config :phx_test, PhxTest.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "shadowlegend",
  password: "Legendary*%23",
  hostname: "database",
  database: "phx-test-prod",
  port: 5432,
  pool_size: 15